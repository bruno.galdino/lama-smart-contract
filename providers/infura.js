const dotenv = require('dotenv');
const HDWalletProvider = require('@truffle/hdwallet-provider');

dotenv.config();

module.exports = (network) => {
  const projectId = process.env.INFURA_PROJECT_ID;
  const mnemonicPhrase = process.env.MNEMONIC_PHRASE;
  const mnemonicPassword = process.env.MNEMONIC_PASSWORD;

  return new HDWalletProvider({
    mnemonic: {
      phrase: mnemonicPhrase,
      password: mnemonicPassword,
    },
    providerOrUrl: `wss://${network}.infura.io/ws/v3/${projectId}`,
    derivationPath: "m/44'/60'/0'/1/0", // first external address (hotwallet)
  });
};
