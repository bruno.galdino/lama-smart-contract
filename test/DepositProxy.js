const assert = require('assert');
const truffleAssert = require('truffle-assertions');
const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const DepositProxy = artifacts.require('DepositProxy');
const DepositOracle = artifacts.require('DepositOracle');
const FakeERC20 = artifacts.require('FakeERC20');

contract('DepositProxy', (accounts) => {
  const owner = accounts[0];

  let Hotwallet;
  let DepositOracleInstance;
  let DepositProxyInstance;

  beforeEach(async () => {
    Hotwallet = web3.eth.accounts.create();

    DepositOracleInstance = await deployProxy(DepositOracle, [[Hotwallet.address]]);
    DepositProxyInstance = await deployProxy(DepositProxy, [DepositOracleInstance.address, []]);
  });

  it('Should not initialize a second time', async () => {
    await truffleAssert.reverts(
      DepositProxyInstance.initialize(DepositOracleInstance.address, [], { from: owner }),
      'Initializable: contract is already initialized',
    );
  });

  it('Should forward all the eth to hotwallet', async () => {
    const amountToForward = web3.utils.toWei('1.232', 'ether');

    await web3.eth.sendTransaction({
      value: amountToForward,
      from: owner,
      to: DepositProxyInstance.address,
    });

    const balanceOfHotwallet = await web3.eth.getBalance(Hotwallet.address);

    assert.equal(balanceOfHotwallet, amountToForward, 'hotwallet should get the eth');
  });

  it('Should withdraw the erc20 tokens to hotwallet', async () => {
    const amountToForward = web3.utils.toWei('1.232', 'ether');

    const fakeErc20 = await FakeERC20.deployed();

    await fakeErc20.transfer(DepositProxyInstance.address, amountToForward, { from: owner });

    await DepositProxyInstance.erc20Withdraw([fakeErc20.address]);

    const balanceOfHotwallet = await fakeErc20.balanceOf.call(Hotwallet.address);

    assert.equal(balanceOfHotwallet, amountToForward, 'hotwallet should get the tokens');
  });
});
