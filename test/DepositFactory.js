const { assert } = require('chai');
const truffleAssert = require('truffle-assertions');
const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

const DepositFactory = artifacts.require('DepositFactory');
const DepositProxy = artifacts.require('DepositProxy');
const DepositOracle = artifacts.require('DepositOracle');
const FakeERC20 = artifacts.require('FakeERC20');

contract('DepositFactory', (accounts) => {
  const owner = accounts[0];
  const notOwner = accounts[9];

  let Hotwallet;
  let DepositOracleInstance;
  let DepositProxyInstance;
  let DepositFactoryInstance;
  let Salt;

  beforeEach(async () => {
    Hotwallet = web3.eth.accounts.create();
    Salt = `${Math.random() * 100000}`;

    DepositOracleInstance = await deployProxy(DepositOracle, [[Hotwallet.address]]);
    DepositProxyInstance = await deployProxy(DepositProxy, [DepositOracleInstance.address, []]);
    DepositFactoryInstance = await deployProxy(DepositFactory, [DepositOracleInstance.address]);
  });

  it('Should be Upgradeable', async () => {
    const upgrade = await upgradeProxy(DepositFactoryInstance.address, DepositFactory);
    assert.strictEqual(
      await upgrade.oracle.call(),
      DepositOracleInstance.address,
      'fail to upgrade',
    );
  });

  it('Should Compute a smart account address', async () => {
    const computedAddress = await DepositFactoryInstance.compute.call(Salt, { from: owner });
    assert.strictEqual(web3.utils.isAddress(computedAddress), true, 'failed to compute address');
  });

  it('Should Deploy a smart account address', async () => {
    const computedAddress = await DepositFactoryInstance.compute.call(Salt, { from: owner });
    assert.strictEqual(web3.utils.isAddress(computedAddress), true, 'failed to compute address');

    const deploy = await DepositFactoryInstance.deploy(Salt, [], { from: owner });

    truffleAssert.eventEmitted(deploy, 'Deployed', (ev) => ev.contractAddress === computedAddress);
  });

  it('Should Not Deploy a smart account with same salt', async () => {
    const computedAddress = await DepositFactoryInstance.compute.call(Salt, { from: owner });
    assert.strictEqual(web3.utils.isAddress(computedAddress), true, 'failed to compute address');

    await DepositFactoryInstance.deploy(Salt, [], { from: owner });

    await truffleAssert.reverts(
      DepositFactoryInstance.deploy(Salt, [], { from: owner }),
      'ERC1167: create2 failed',
    );
  });

  it('Should forward all the eth to hotwallet on creation', async () => {
    const amountToForward = web3.utils.toWei('1.232', 'ether');
    const computedAddress = await DepositFactoryInstance.compute.call(Salt, { from: owner });

    await web3.eth.sendTransaction({
      value: amountToForward,
      from: owner,
      to: computedAddress,
    });

    await DepositFactoryInstance.deploy(Salt, [], { from: owner });

    const balanceOfHotwallet = await web3.eth.getBalance(Hotwallet.address);

    assert.equal(balanceOfHotwallet, amountToForward, 'hotwallet should get the eth');
  });

  it('Should forward all the erc20 tokens to hotwallet on creation', async () => {
    const amountToForward = web3.utils.toWei('1.232', 'ether');
    const computedAddress = await DepositFactoryInstance.compute.call(Salt, { from: owner });

    const fakeErc20 = await FakeERC20.deployed();

    await fakeErc20.transfer(computedAddress, amountToForward, { from: owner });

    await DepositFactoryInstance.deploy(Salt, [fakeErc20.address], { from: owner });

    const balanceOfHotwallet = await fakeErc20.balanceOf.call(Hotwallet.address);

    assert.equal(balanceOfHotwallet, amountToForward, 'hotwallet should get the eth');
  });

  it('Should revert if not an DEPLOYER deploying contract', async () => {
    await truffleAssert.reverts(
      DepositFactoryInstance.compute.call(Salt, { from: notOwner }),
      'Not allowed',
    );
  });

  it('Should revert if not an DEPLOYER deploying contract', async () => {
    await truffleAssert.reverts(
      DepositFactoryInstance.deploy(Salt, [], { from: notOwner }),
      'Not allowed',
    );
  });

  it('Should GRANT deployer role to an address', async () => {
    await DepositFactoryInstance.grantDeployer(notOwner, { from: owner });
    await DepositFactoryInstance.deploy(Salt, [], { from: notOwner });
  });

  it('Should REVOKE deployer role to an address', async () => {
    await DepositFactoryInstance.revokeDeployer(notOwner, { from: owner });
    await truffleAssert.reverts(
      DepositFactoryInstance.deploy(Salt, [], { from: notOwner }),
      'Not allowed',
    );
  });

  it('Should NOT GRANT deployer role if not admin role', async () => {
    await truffleAssert.reverts(
      DepositFactoryInstance.grantDeployer(notOwner, { from: notOwner }),
      'Not allowed',
    );
  });

  it('Should NOT REVOKE deployer role if not admin role', async () => {
    await truffleAssert.reverts(
      DepositFactoryInstance.revokeDeployer(notOwner, { from: notOwner }),
      'Not allowed',
    );
  });
});
