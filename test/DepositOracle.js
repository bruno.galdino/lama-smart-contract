const assert = require('assert');
const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');
const truffleAssert = require('truffle-assertions');

const DepositOracle = artifacts.require('DepositOracle');

const firstDestination = '0x4DDD18d0bb4a9C2FE1F3489B62D3d68A0E31753A';
const secondDestination = '0x47E7Ba2E8b2B1774365F12a9C8A78Fe41413eb87';
const thirdDestination = '0xDF2d1b31801D0d7B50070EAB5e820c6A6393A6DD';

contract('DepositOracle', (accounts) => {
  let DepositOracleInstance;

  beforeEach(async () => {
    DepositOracleInstance = await deployProxy(DepositOracle, [[firstDestination]]);
  });

  it('Should be Upgradeable', async () => {
    const upgrade = await upgradeProxy(DepositOracleInstance.address, DepositOracle);
    assert.equal(await upgrade.getDestination.call(0), firstDestination, 'fail to upgrade');
  });

  it('Number of destinations should be 1', async () => {
    const numberOfDestinations = await DepositOracleInstance.getDestinationsCount.call();
    assert.equal(numberOfDestinations, 1, 'Number of destinations should be 1');
  });

  it(`First destination should be ${firstDestination}`, async () => {
    const destination = await DepositOracleInstance.getDestination.call(0);
    assert.equal(destination, firstDestination, `Destination should be ${firstDestination}`);
  });

  it(`Shoud add/remove second destination ${secondDestination}`, async () => {
    await DepositOracleInstance.addDestinations([secondDestination], { from: accounts[0] });

    assert.equal(await DepositOracleInstance.getDestinationsCount.call(), 2, 'It should be 2');

    assert.equal(
      await DepositOracleInstance.getDestination.call(1),
      secondDestination,
      `It should be ${secondDestination}`,
    );

    await DepositOracleInstance.removeDestinations([secondDestination], { from: accounts[0] });

    assert.equal(
      await DepositOracleInstance.getDestinationsCount.call(),
      1,
      'It should be 1 again',
    );

    assert.equal(
      await DepositOracleInstance.getDestination.call(0),
      firstDestination,
      `It should be ${firstDestination}`,
    );
  });

  it('Should revert trying to remove last destination', async () => {
    await truffleAssert.reverts(
      DepositOracleInstance.removeDestinations([firstDestination], { from: accounts[0] }),
      'At least one destination must remain',
    );
  });

  it(`Next Destination Should be ${firstDestination}`, async () => {
    const next = await DepositOracleInstance.nextDestination({ from: accounts[0] });

    truffleAssert.eventEmitted(
      next,
      'NextDestinationEvent',
      (ev) => ev.destination === firstDestination,
    );
  });

  it('Next Destination Should Round Robbin', async () => {
    await DepositOracleInstance.addDestinations([secondDestination, thirdDestination], {
      from: accounts[0],
    });

    const next = await DepositOracleInstance.nextDestination({ from: accounts[0] });

    truffleAssert.eventEmitted(
      next,
      'NextDestinationEvent',
      (ev) => ev.destination === firstDestination,
    );

    const next2 = await DepositOracleInstance.nextDestination({ from: accounts[0] });

    truffleAssert.eventEmitted(
      next2,
      'NextDestinationEvent',
      (ev) => ev.destination === secondDestination,
    );

    const next3 = await DepositOracleInstance.nextDestination({ from: accounts[0] });

    truffleAssert.eventEmitted(
      next3,
      'NextDestinationEvent',
      (ev) => ev.destination === thirdDestination,
    );

    await DepositOracleInstance.removeDestinations([firstDestination], { from: accounts[0] });

    const next4 = await DepositOracleInstance.nextDestination({ from: accounts[0] });

    truffleAssert.eventEmitted(
      next4,
      'NextDestinationEvent',
      (ev) => ev.destination === thirdDestination,
    );

    const next5 = await DepositOracleInstance.nextDestination({ from: accounts[0] });

    truffleAssert.eventEmitted(
      next5,
      'NextDestinationEvent',
      (ev) => ev.destination === secondDestination,
    );
  });
});
