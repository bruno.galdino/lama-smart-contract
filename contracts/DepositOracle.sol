// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import '@openzeppelin/contracts-upgradeable/utils/structs/EnumerableSetUpgradeable.sol';
import '@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol';
import '@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol';

/**
 * Oracle Contract.
 * It defines the address of hotwallets described here as "desitinations"
 * This contract MUST only be executed by an admin member
 */
contract DepositOracle is Initializable, OwnableUpgradeable {
  using EnumerableSetUpgradeable for EnumerableSetUpgradeable.AddressSet;

  event NextDestinationEvent(address destination);

  /**
   * Unsigned integer index used to choose the next destination to use as hotwallet
   */
  uint256 private nextDestinationIndex;

  /**
   * Set of added destinations
   */
  EnumerableSetUpgradeable.AddressSet private destinations;

  /**
   * On contract creation, it MUST set at least one destination address
   * @param _destinations {address[]}
   */
  function initialize(address[] memory _destinations) public initializer {
    __Ownable_init();
    _addDestinations(_destinations);
  }

  /**
   * Return the next hotwallet as the destination
   * If there is only one, returns it and ends execution in order to save some gas.
   */
  function nextDestination() external returns (address payable) {
    return destinations.length() == 1 ? _nextDestination(false) : _nextDestination(true);
  }

  /**
   * Return the next hotwallet as the destination
   * Increment de destinations index if _increment is true
   * @param _increment {bool} wheter of not to increment nextDestinationIndex
   */
  function _nextDestination(bool _increment) internal returns (address payable) {
    address payable next = payable(destinations.at(nextDestinationIndex));

    if (_increment) {
      nextDestinationIndex += 1;

      if (nextDestinationIndex >= destinations.length()) {
        nextDestinationIndex = 0;
      }
    }

    emit NextDestinationEvent(next);
    return next;
  }

  /**
   * ONLY contract owner MAY execute this function
   * @param _destinations {address[]} list of hotwallet addresses
   */
  function addDestinations(address[] memory _destinations) external onlyOwner {
    _addDestinations(_destinations);
  }

  /**
   * Add at least one destination
   * Destinations cannot be the 0x0 address
   * @param _destinations {address[]} list of destinations to add
   */
  function _addDestinations(address[] memory _destinations) internal {
    require(_destinations.length >= 1, 'At least one destination is needed');
    require(_destinations.length <= 250, 'Too many destinations');

    for (uint256 i = 0; i < _destinations.length; i++) {
      require(_destinations[i] != address(0), 'Invalid address');
      destinations.add(_destinations[i]);
    }
  }

  /**
   * Remove destinations
   * ONLY contract owner MAY execute this function
   * @param _destinations {address[]} list of destinations to remove
   */
  function removeDestinations(address[] memory _destinations) external onlyOwner {
    _removeDestinations(_destinations);
  }

  /**
   * Removes at leat one hotwallet of the destination list
   * @param _destinations {address[]} list of destinations to remove
   */
  function _removeDestinations(address[] memory _destinations) internal {
    require(_destinations.length >= 1, 'At least one _destinations is needed');
    require(_destinations.length <= 250, 'Too many _destinations');
    require(_destinations.length < destinations.length(), 'At least one destination must remain');

    for (uint256 i = 0; i < _destinations.length; i++) {
      destinations.remove(_destinations[i]);
    }
  }

  /**
   * Returns one destination given an index
   * @param _index {uint}
   */
  function getDestination(uint256 _index) external view returns (address) {
    return address(destinations.at(_index));
  }

  /**
   * Returns number of hotwallets on destinations list
   */
  function getDestinationsCount() external view returns (uint256) {
    return uint256(destinations.length());
  }
}
