// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import './DepositOracle.sol';
import '@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol';
import '@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol';

/**
 * Deposit Proxy contract
 * This contract is used as an implementation of a smart account
 * It is supposed to be deployed by a Contract Factory eg: DepositFactory.sol
 */
contract DepositProxy is Initializable {
  /**
   * Reference address of oracle contract
   * Oracle contract will decide which hotwallet to transfer the funds to
   */
  address public oracle;

  /**
   * Initialize the contract, this function MUST only be called once
   * On initialize eth and given erc20 tokens balances should be forwarded to a hotwallet
   * The address of the hot wallet should be defined by the oracle contract
   * @param _oracle {address} DepositOracle address
   * @param _tokens {address[]} List of ERC20 tokens to make transfer
   */
  function initialize(address _oracle, address[] memory _tokens) public initializer {
    require(_oracle != address(0), 'invalid oracle address');
    require(_tokens.length <= 250, 'Too many tokens');

    oracle = _oracle;

    // Get hotwallet address from oracle
    address payable hotwallet = _getOracleDestination();

    // send all tokens to hotwallet
    for (uint256 i = 0; i < _tokens.length; i++) {
      _erc20Withdraw(hotwallet, _tokens[i]);
    }

    // Send all eth balance to hotwallet
    _ethWithdraw(hotwallet);
  }

  /**
   * Default receive function
   * Ether sent to this contract will fallback to it
   * Every ether sent is then forward to orcale's hotwallet
   */
  receive() external payable {
    // Get hotwallet address from oracle
    address payable hotwallet = _getOracleDestination();

    // Send all eth balance to hotwallet
    _ethWithdraw(hotwallet);
  }

  /**
   * Withdraw ERC20 tokens to hotwallets
   * This function MAY be called by anyone,
   * since Tokens will always be sent to oracle's hotwallet
   * @param _tokens {address[]}
   */
  function erc20Withdraw(address[] memory _tokens) public {
    require(_tokens.length <= 250, 'Too many tokens');
    // Get hotwallet address from oracle
    address payable hotwallet = _getOracleDestination();

    // send all tokens to hotwallet
    for (uint256 i = 0; i < _tokens.length; i++) {
      _erc20Withdraw(hotwallet, _tokens[i]);
    }
  }

  /**
   * Returns the oracle's destination to send the funds to
   * if there is only one destination in the oracle, returns it
   */
  function _getOracleDestination() internal returns (address payable) {
    return payable(DepositOracle(oracle).nextDestination());
  }

  /**
   * Withdraw ERC20 tokens to destination
   * This is an internal function,
   * @param _destination {address payable}
   * @param _token {address}
   */
  function _erc20Withdraw(address payable _destination, address _token) internal {
    // send all tokens to hotwallet
    require(_token != address(0), 'Invalid token address');

    IERC20Upgradeable token = IERC20Upgradeable(_token);
    bool success = token.transfer(_destination, token.balanceOf(address(this)));
    require(success, 'transfer failed');
  }

  /**
   * Send all contract balance to _destination
   * @param _destination {address payable}
   */
  function _ethWithdraw(address payable _destination) internal {
    // Send all eth balance to _destination
    (bool success, ) = _destination.call{value: address(this).balance}('');
    require(success, 'transfer failed');
  }
}
