// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import './DepositProxy.sol';
import '@openzeppelin/contracts-upgradeable/proxy/ClonesUpgradeable.sol';
import '@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol';
import '@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol';

/**
 * Deposit Factory
 * Contract responsible to compute and deploy smart contracts
 * using counterfactual method (create2)
 *
 * @dev This is an upgradable contract using contracts-upgradeable from Openzeppeling
 */
contract DepositFactory is Initializable, AccessControlUpgradeable {
  bytes32 public constant DEPLOYER_ROLE = keccak256('DEPLOYER_ROLE');
  address public implementation;
  address public oracle;

  event Deployed(address contractAddress);

  /**
   * The sender must be granted given '_role' before using the especified method
   * @param _role {bytes32} Required role
   */
  modifier allowedRole(bytes32 _role) virtual {
    require(hasRole(_role, msg.sender), 'Not allowed');
    _;
  }

  /**
   * Intialize contract
   * Setup sender roles and implementation adress, as well as oracle address
   * @dev This is an upgradable contract using contracts-upgradeable from Openzeppeling
   * this function must be called only once on contract deployment, equivalent to constructor.
   * @param _oracle {address}
   */
  function initialize(address _oracle) public initializer {
    __AccessControl_init();
    _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
    _setupRole(DEPLOYER_ROLE, msg.sender);
    implementation = address(new DepositProxy());
    oracle = _oracle;
  }

  /**
   * Compute the smart contract address to be deployed
   * @dev this is an idempotent call, based on '_salt' the resulted address will always be the same
   * @param _salt {string} random string to be used as salt
   * @dev sender must be previously granted the DEPLOYER_ROLE to execute this call
   */
  function compute(string memory _salt) public view allowedRole(DEPLOYER_ROLE) returns (address) {
    return
      ClonesUpgradeable.predictDeterministicAddress(
        implementation,
        keccak256(abi.encodePacked(_salt))
      );
  }

  /**
   * Deploy and initialize the Proxy contract on pre-computed address
   * @param _salt {string} random string to be used as salt
   * @param _tokens {address[]} ERC20 tokens to be withdrawn from the Deposit proxy
   * @dev sender must be previously granted the DEPLOYER_ROLE to execute this method
   */
  function deploy(string memory _salt, address[] memory _tokens)
    public
    allowedRole(DEPLOYER_ROLE)
    returns (address target)
  {
    target = ClonesUpgradeable.cloneDeterministic(
      implementation,
      keccak256(abi.encodePacked(_salt))
    );
    DepositProxy(payable(target)).initialize(oracle, _tokens);
    emit Deployed(target);
  }

  /**
   * Grants an address the DEPLOYER_ROLE
   * @param deployer {address}
   * @dev sender must be previously granted the DEFAULT_ADMIN_ROLE to execute this method
   */
  function grantDeployer(address deployer) public allowedRole(DEFAULT_ADMIN_ROLE) {
    grantRole(DEPLOYER_ROLE, deployer);
  }

  /**
   * Revokes the DEPLOYER_ROLE from an deployer
   * @param deployer {address}
   * @dev sender must be previously granted the DEFAULT_ADMIN_ROLE to execute this method
   */
  function revokeDeployer(address deployer) public allowedRole(DEFAULT_ADMIN_ROLE) {
    revokeRole(DEPLOYER_ROLE, deployer);
  }
}
