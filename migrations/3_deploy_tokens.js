const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const FakeERC20 = artifacts.require('FakeERC20');

module.exports = async function (deployer) {
  const { from } = deployer.networks.development;

  if (process.env.NODE_ENV === 'test') {
    await deployProxy(FakeERC20, ['Fake ERC20', 'FAKE', '1000000000000000000000', from], {
      deployer,
    });
  }
};
