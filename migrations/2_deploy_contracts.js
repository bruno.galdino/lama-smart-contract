const dotenv = require('dotenv');
const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const DepositOracle = artifacts.require('DepositOracle');
const DepositProxy = artifacts.require('DepositProxy');
const DepositFactory = artifacts.require('DepositFactory');

dotenv.config();

module.exports = async function (deployer) {
  const oracle = await deployProxy(DepositOracle, [[deployer.networks.development.from]], {
    deployer,
  });
  const proxy = await deployProxy(DepositProxy, [oracle.address, []], { deployer });
  const factory = await deployProxy(DepositFactory, [oracle.address], { deployer });

  console.log({ oracle: oracle.address, proxy: proxy.address, factory: factory.address });
};
