const web3utils = require('web3-utils');
const dotenv = require('dotenv');
const infuraProvider = require('./providers/infura');

dotenv.config();

const gasPrice = web3utils.toWei(process.env.GAS_PRICE_GWEI, 'Gwei');
const kovanProvider = infuraProvider('kovan');
const mainnetProvider = infuraProvider('mainnet');

module.exports = {
  compilers: {
    solc: {
      version: '0.8.4',
      settings: {
        optimizer: {
          enabled: true,
          runs: 200,
        },
      },
    },
  },

  networks: {
    development: {
      host: '127.0.0.1',
      port: 8545,
      gasPrice,
      network_id: '*',
    },

    kovan: {
      provider() {
        return kovanProvider;
      },
      from: kovanProvider.getAddress(0),
      gasPrice,
      timeoutBlocks: 200,
      cofirmations: 1,
      network_id: 42,
    },

    mainnet: {
      provider() {
        return mainnetProvider;
      },
      from: mainnetProvider.getAddress(0),
      gasPrice,
      timeoutBlocks: 200,
      cofirmations: 1,
      network_id: 1,
    },
  },

  mocha: {
    reporter: 'eth-gas-reporter',
    reporterOptions: {
      currency: 'EUR',
      coinmarketcap: 'da777b65-96ca-40f5-805b-a4d96a84ecf7',
      excludeContracts: ['Migrations'],
      gasPrice: process.env.GAS_PRICE_GWEI,
    },
  },
};
