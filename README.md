# deposit-proxy-smart-contracts

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Smart Contracts to forward deposits to Lama hotwallets


## Table of Contents

- [Environment](#environment)
- [Install](#install)
- [Usage](#usage)
- [Test](#test)
- [Migrations](#migrations)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Environment

First we need to setup the environment variables to be able to run tests and migrations.

Create a `.env` file with the contents of `.env.sample`. then simply fill the values of the variables.

```
cp .env.sample .env
```

## Install
To install dependencies we simply run a yarn / npm install

```
yarn install
```

## Test
Tests are executed with `truffle test` and we use `eth-gas-reporter` to show estimation of gas usage

```
yarn test
```

## Migrations

To migrate to local development blockchain, first run `truffle develop`or `ganache-cli` then run the `migrate:local`script. Local development network should run on port `8545`
```
yarn truffle develop (in another tab)
yarn migrate:local
```

for migrations on `testnet` or `mainnet`, first it makes a dry-run, then it asks for a confirmation to publish the migration to the network

To run migration on kovan testnet run:
```
yarn migrate:develop
```

To run migration on mainnet run:
```
yarn migrate:production
```


## Maintainers

[bruno.c@piercetechnology.co.uk](https://gitlab.com/CamposBruno)

## Contributing



Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2021 piercetechnology.co.uk
